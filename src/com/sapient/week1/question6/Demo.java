package com.sapient.week1.question6;

public class Demo 
{

	public static void main(String[] args) 
	{
		AddBean addbean = new AddBean();
		View view =  new View();
		AddDAO adddao = new AddDAO();
		
		view.readData(addbean);
		adddao.compute(addbean);
		
		addbean = null;
		view = null;
		adddao = null;
		
		System.gc();
	}

}
