package com.sapient.week1.question6;

public class AddDAO 
{
	public void compute(AddBean ob)
	{
		ob.setNum3(ob.getNum1()+ob.getNum2());
	}
	public void finalize()
	{
		System.out.println("AddDAO object is destroyed.");
	}

}
