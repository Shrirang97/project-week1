package com.sapient.week1.question14;

public class TockPlayer extends Thread {
	
	final private String name;
	private Object obj;
	
	public TockPlayer(Object obj) 
	{
		this.name = "Tock";
		this.obj = obj;
	}
	public synchronized void tock()
	{
		while(true)
			synchronized(obj)
			{
				
				{
					System.out.println(name);
					obj.notify();
					try 
					{
						obj.wait();
					} 
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
				}
				
			}
	}
	@Override
	public void run() {
		tock();
	}

}
