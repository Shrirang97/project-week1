package com.sapient.week1.question14;

public class TickPlayer extends Thread {

	private String name;
	private Object obj;
	
	public TickPlayer(Object obj) 
	{
		name = "Tick";
		this.obj = obj;
	}
	public synchronized void tick()
	{
		while(true)
			synchronized(obj)
			{
				{
					System.out.println(name);
					obj.notify();
					try 
					{
						obj.wait();
					} 
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}

				}
				
			}
	}
	@Override
	public void run() 
	{
		tick();
	}
	
}
