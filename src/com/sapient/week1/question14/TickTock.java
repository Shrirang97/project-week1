package com.sapient.week1.question14;

public class TickTock {

	public static void main(String[] args) {
		Object obj = new Object();
		TickPlayer tick = new TickPlayer(obj);
		TockPlayer tock = new TockPlayer(obj);

		tick.start();
		tock.start();
		
		

	}

}
