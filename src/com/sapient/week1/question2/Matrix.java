package com.sapient.week1.question2;

import com.sapient.week1.Read;

public class Matrix {
	
	private int mat[][];
	private int m = 3;
	private int n = 3;
	
	Matrix()
	{	
		mat = new int[m][n];
	}
	
	Matrix(int m,int n)
	{
		setM(m);
		setN(n);
		mat = new int[m][n];
	}

	Matrix(int m[][])
	{
		setM(m.length);
		setN(m[0].length);
		setMat(m);
	}
	
	Matrix(Matrix matrix)
	{
		setM(matrix.m);
		setN(matrix.n);
		setMat(matrix.mat);
	}

	public int[][] getMat() {
		return mat;
	}

	public void setMat(int[][] mat) {
		this.mat = mat;
	}

	public int getM() {
		return m;
	}

	public void setM(int m) {
		this.m = m;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}
	
	public void display()
	{
		for(int i=0;i<m;i++)
		{
			for(int j=0;j<n;j++)
			{
				System.out.print(mat[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("\n\n");
	}
	
	public void read()
	{
		System.out.println("Enter matrix dimensions: ");
		this.m = Read.sc.nextInt();
		this.n = Read.sc.nextInt();
		System.out.println("Enter matrix: ");
		for(int i=0;i<m;i++)
		{
			for(int j=0;j<n;j++)
			{
				mat[i][j] = Read.sc.nextInt();
			}
		}
	}
	
	public Matrix add(Matrix matrix)
	{
		if(this.m!=matrix.m || this.n!=matrix.n)
		{
			System.out.println("Addition is not possible");
			return (Matrix) new Object();
		}
		Matrix temp = new Matrix(this.m,this.n);
		for(int i=0;i<m;i++)
		{
			for(int j=0;j<n;j++)
			{
				temp.mat[i][j] = this.mat[i][j] + matrix.mat[i][j]; 
			}
		}
		return temp;
	}
	
	public Matrix multiply(Matrix matrix)
	{
		Matrix temp = new Matrix(this.m,this.n);
		if(this.n!=matrix.m)
		{
			System.out.println("Multiplication is not possible. Check Dimensions!");
			return (Matrix) new Object();
		}
		for(int i=0;i<this.m;i++)
		{
			for(int j=0;j<matrix.n;j++)
			{
				for(int k=0;k<this.n;k++)
					temp.mat[i][j] += this.mat[i][k]*matrix.mat[k][j]; 
			}
		}
		return temp;
	}

}
