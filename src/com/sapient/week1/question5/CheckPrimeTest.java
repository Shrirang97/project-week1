package com.sapient.week1.question5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CheckPrimeTest {
	private CheckPrime obj = new CheckPrime();
	@Test
	void testCheckPrime() {
		assertEquals(false, obj.checkPrime(0));
	}
	@Test
	void testCheckPrime0() {
		assertEquals(false, obj.checkPrime(1));
	}
	@Test
	void testCheckPrime1() {
		assertEquals(true, obj.checkPrime(2));
	}
	@Test
	void testCheckPrime2() {
		assertEquals(true, obj.checkPrime(3));
	}
	@Test
	void testCheckPrime3() {
		assertEquals(false, obj.checkPrime(9));
	}
	

}
