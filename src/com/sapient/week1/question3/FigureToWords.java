package com.sapient.week1.question3;

public class FigureToWords {

	public static String getWords(long amt)
	{
		String word = "";
		String unit[] = {"","One","Two","Three","Four","Five","Six","Seven"
				,"Eight","Nine","Ten","Eleven","Twelve","Thirteen","Fourteen"
				,"Fifteen","Sixteen","Seventeen","Eighteen","Nineteen"};
		String tens[] = {"","Ten","Twenty","Thirty","Fourty","Fifty","Sixty"
				,"Seventy","Eighty","Ninty"};
		String vunit[] = {"Crores","Lakhs","Thousands","Hundred","Only"};
		long nunit[] = {10000000L,100000L,1000L,100L,1L};
		for(int i=0;i<nunit.length;i++)
		{
			int n = (int)(amt/nunit[i]);
			amt = amt%nunit[i];
			if(n>0)
			{
				if(n>19)
				{
					word += tens[n/10] +" "+ unit[n%10] +" "+ vunit[i];
				}
				else
				{
					word += unit[n]+" "+vunit[i];
				}
			}
		}
		
		
		
		
		return word;
	}
}
