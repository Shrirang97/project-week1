package com.sapient.week1.question1;

public class Demo {

	public static void main(String[] args) {
		
		int a[] = {4,5,6,1,3,2};
		IntegerArray obj = new IntegerArray(a);
		System.out.println(obj.search(5));
		obj.sort();
		System.out.println(obj.average());
		obj.display();

	}

}
