package com.sapient.week1.question10;

import java.io.*;

public class FileAdd {

	public static void main(String[] args) throws Exception{
		File f1 = new File("C:\\Users\\shrpinja\\training\\day-3\\note1.csv");
		FileReader fr = new FileReader(f1);
		BufferedReader bfr = new BufferedReader(fr);
		String line = "";
		File f2 = new File("C:\\Users\\shrpinja\\training\\day-3\\note2.txt");
		FileWriter fw = new FileWriter(f2);
		while( (line = bfr.readLine())!=null)
		{
			String arr[] = line.split(",");
			int sum = 0;
			for(String s: arr)
			{
				sum += Integer.parseInt(s);
			}
			line += "="+sum+"\n";
			fw.write(line);
		}
		fr.close();
		fw.close();
		bfr.close();
				
		
	}

}
