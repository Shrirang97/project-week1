package com.sapient.week1.question9;

import java.util.ArrayList;
import java.util.Collections;

public class Demo1 {

	public static void main(String[] args) 
	{
		ArrayList<Employee> list = new ArrayList<Employee>();
		
		Employee e1 = new Employee(1,"Sam", 22);
		Employee e2 = new Employee(2,"Dave", 21);
		Employee e3 = new Employee(3,"Paul", 25);
		Employee e4 = new Employee(4,"Olive", 24);
		Employee e5 = new Employee(5,"Mark", 23);
		
		list.add(e1);
		list.add(e2);
		list.add(e3);
		list.add(e4);
		list.add(e5);
		
		Collections.sort(list, new NameSorter());
		
		for(Employee e: list)
		{
			System.out.println(e);
		}
		
	}

}
