package com.sapient.week1.question9;

import java.util.ArrayList;
import java.util.Collections;

public class Demo {

	public static void main(String[] args) 
	{
		ArrayList hmap = new ArrayList<Employee>();
		
		Employee e1 = new Employee(1,"Sam", 22);
		Employee e2 = new Employee(2,"Dave", 21);
		Employee e3 = new Employee(3,"Paul", 25);
		Employee e4 = new Employee(4,"Olive", 24);
		Employee e5 = new Employee(5,"Mark", 23);
		
		hmap.add(12, e1);
		hmap.add(11, e2);
		hmap.add(15, e3);
		hmap.add(14, e4);
		hmap.add(13, e5);
		
		Collections.sort(hmap, new NameSorter());
		
		
		
	}

}
