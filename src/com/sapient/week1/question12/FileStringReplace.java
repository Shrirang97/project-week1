package com.sapient.week1.question12;

import java.io.*;

import com.sapient.week1.Read;

public class FileStringReplace {

	public static void main(String[] args) throws Exception{
		File f1 = new File("C:\\Users\\shrpinja\\training\\day-3\\note1.csv");
		FileReader fr = new FileReader(f1);
		BufferedReader bfr = new BufferedReader(fr);
		String line = "";
		File f2 = new File("C:\\Users\\shrpinja\\training\\day-3\\note3.txt");
		FileWriter fw = new FileWriter(f2);
		System.out.println("Enter word : ");
		String search = Read.sc.next();
		while( (line = bfr.readLine())!=null)
		{
			if(line.contains(search))
			{
				System.out.println(line);
				line = line.replace(search, "###");
			}
			fw.write(line+"\n");
		}
		fr.close();
		fw.close();
		bfr.close();
				
		
	}

}
