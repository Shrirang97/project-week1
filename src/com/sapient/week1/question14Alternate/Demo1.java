package com.sapient.week1.question14Alternate;

import com.sun.swing.internal.plaf.synth.resources.synth;

class Game
{
	public synchronized void g1()
	{
		try
		{
			notify();
			System.out.print(" Tick -");
			wait();
		}catch (Exception e) {
			
		}
	}
	
	public synchronized void g2()
	{
		try
		{
			notify();
			System.out.println(" Tock ");
			wait();
		}catch (Exception e) {
			
		}
	}
}
class Player1 extends Thread
{
	Game ob;
	public Player1(Game ob)
	{
		this.ob = ob;
	}
	
	@Override
	public void run() 
	{
		for(int i=0;i<3;i++)
		{
			ob.g1();
		}
	}
}
class Player2 extends Thread
{
	Game ob;
	public Player2(Game ob)
	{
		this.ob = ob;
	}
	
	@Override
	public void run() 
	{
		for(int i=0;i<3;i++)
		{
			if(i==0)
			{
				try {
					sleep(10);
				} catch (InterruptedException e) {
				e.printStackTrace();
				}
			}
			ob.g2();
		}
	}
}
public class Demo1 
{

	public static void main(String[] args) 
	{
		Game game = new Game();
		Player1 p1 = new Player1(game);
		Player2 p2 = new Player2(game);
		
		p1.start();
		p2.start();

	}

}
