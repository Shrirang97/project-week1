package com.sapient.week1.question16;

import java.util.ArrayList;

public class EmployeeData 
{
	ArrayList elist;
	
	public EmployeeData() 
	{
		elist = new ArrayList<Employee>();
	}
	
	public void add(Employee e)
	{
		elist.add(e);
	}
	
	public Employee get(int i) 
	{
		return (Employee)elist.get(i);
	}
	
}
