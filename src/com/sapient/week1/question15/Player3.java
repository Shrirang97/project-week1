package com.sapient.week1.question15;

public class Player3 extends Thread implements Player
{
	Game game;
	public Player3(Game game)
	{
		this.game = game;
	}
	
	@Override
	public void run() 
	{
		while(true)
		{
			game.g3();
		}
	}
}
