package com.sapient.week1.question15;

public class Game 
{
	private int value = 1;
	public synchronized void g1()
	{
		//System.out.println(Thread.currentThread().getId()+" g1 "+value);
		//System.out.println(1);
		try
		{
			if(value == 1)
			{ 
				this.notifyAll();
				System.out.println(" Player 1 ");
				value = 2;
			}
			else
			{
				this.wait();
			}
			
			
		}catch (Exception e) {
			System.out.println("Exception");
		}
	}
	
	public synchronized void g2()
	{
		//System.out.println(Thread.currentThread().getId()+" g2 "+value);
		//System.out.println(2);
		try
		{
			if(value == 2)
			{ 
				this.notifyAll();
				System.out.println(" Player 2 ");
				value = 3;
			}
			else
			{
				this.wait();
			}
		}catch (Exception e) {
			System.out.println("Exception");
		}
	}
	
	public synchronized void g3()
	{
		//System.out.println(Thread.currentThread().getId()+" g3 "+value);
		//System.out.println(3);
		try
		{
			if(value == 3)
			{ 
				this.notifyAll();
				System.out.println(" Player 3 ");
				value = 1;
			}
			else
			{
				this.wait();
			}
		}catch (Exception e) {
			System.out.println("Exception");
		}
	}
}
