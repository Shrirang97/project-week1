package com.sapient.week1.question15;

public class Demo {

	public static void main(String[] args) throws InterruptedException
	{
		Game game = new Game();
		Player1 p1 = new Player1(game);
		Player2 p2 = new Player2(game);
		Player3 p3 = new Player3(game);
		System.out.println("Playing Cards Game ");
		p1.start();
		p2.start();
		p3.start();
		
		p1.join();
		p2.join();
		p3.join();
		
		

	}

}
