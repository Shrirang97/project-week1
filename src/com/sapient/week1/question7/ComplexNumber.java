package com.sapient.week1.question7;

public class ComplexNumber {
	
	private float real;
	private float imaginary;
	
	public ComplexNumber(float i, float j) 
	{
		real = i;
		imaginary = j;
	}
	public ComplexNumber()
	{
		
	}
	public float getReal() 
	{
		return real;
	}
	public void setReal(float real) 
	{
		this.real = real;
	}
	public float getImaginary() 
	{
		return imaginary;
	}
	public void setImaginary(float imaginary) 
	{
		this.imaginary = imaginary;
	}
	@Override
	public String toString() {
		
		return "Complex Number : "+ real +" + "+ imaginary + " i";
	}
	public ComplexNumber add(ComplexNumber cn)
	{
		float i = this.real + cn.real;
		float j = this.imaginary + cn.imaginary;
		return new ComplexNumber(i, j);
	}
	
	
	

}
