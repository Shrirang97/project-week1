package com.sapient.week1.question7;

import com.sapient.week1.Read;

public class ComplexNumberView 
{
	public void readComplexNumber(ComplexNumber cn)
	{
		System.out.println("Enter your real part : ");
		cn.setReal(Read.sc.nextFloat());
		System.out.println("Enter your imaginary part : ");
		cn.setImaginary(Read.sc.nextFloat());
	}
}
