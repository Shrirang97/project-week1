package com.sapient.week1.question7;

public class ComplexNumberController {

	public static void main(String[] args) 
	{
		ComplexNumber cn1 = new ComplexNumber();
		ComplexNumber cn2 = new ComplexNumber();
		ComplexNumberAdder cnAdd = new ComplexNumberAdder();

		ComplexNumberView cnView = new ComplexNumberView();
		System.out.println("First Complex Number : ");
		cnView.readComplexNumber(cn1);
		System.out.println("Second Complex Number : ");
		cnView.readComplexNumber(cn2);
		
		System.out.println(cnAdd.add(cn1, cn2));
		
	}

}
