package com.sapient.week1.question7;

public class ComplexNumberAdder 
{
	public ComplexNumber add(ComplexNumber cn1,ComplexNumber cn2)
	{
		float i =  cn1.getReal()+cn2.getReal();
		float j = cn1.getImaginary()+cn2.getImaginary();
		return new ComplexNumber(i, j);
	}
	
}
