package com.sapient.week1.question7;

public class Numbers<T extends Number> 
{
	private T value;
	Numbers(T value)
	{
		this.value = value;
	}
	public Numbers<T> add(Numbers<Integer> t)
	{
		return (Numbers<T>) t;//new Numbers<T>((T)(this.value + t.value));
	}
	public T getValue() {
		return value;
	}
	public void setValue(T value) {
		this.value = value;
	}

}
