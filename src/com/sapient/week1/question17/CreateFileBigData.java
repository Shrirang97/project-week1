package com.sapient.week1.question17;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;

public class CreateFileBigData {

	public static void main(String[] args) {
		
		String s = "C:\\Users\\shrpinja\\Downloads\\1500000 Sales Records\\";
		
		File outputfile = new File(s+"bigdata.csv");
		
		FileWriter fileWriter = null;
		BufferedWriter bufferedWriter = null;
		try {
			fileWriter = new FileWriter(outputfile);
			bufferedWriter = new BufferedWriter(fileWriter);
		} catch (IOException e1) {
			
			e1.printStackTrace();
		}
		
		long time1 = System.nanoTime();
		int num = 1;
		System.out.println("Start : "+time1);
	    try{
	      
	      while(outputfile.length()/(1024*1024*1024) <1)
	      {
	    	  bufferedWriter.write(num +" ^ 2 = "+(int)Math.pow(num,2)+" \n");
	    	  num += 1;
	      }
	    }catch (Exception e) 
	    {
			System.out.println(e);
		}
		long time2 = System.nanoTime();
		System.out.println("End : "+time2);
	    System.out.println("Time took "+((time2-time1)/1e9));

	}

}
