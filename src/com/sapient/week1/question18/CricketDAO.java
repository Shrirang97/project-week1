package com.sapient.week1.question18;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CricketDAO 
{
	public ArrayList<PlayerBean> getPlayers() throws Exception
	{
		Connection con = DBConnect.getConnection();
		PreparedStatement ps = con.prepareStatement("SELECT ID, FIRST_NAME, LAST_NAME, JERSEY_NUMBER FROM PLAYER");
		ResultSet rs = ps.executeQuery();
		ArrayList<PlayerBean> alist = new ArrayList<PlayerBean>();
		
		while(rs.next())
		{
			PlayerBean player = new PlayerBean(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
			alist.add(player);
		}
	    return alist;
	}
	
	public ArrayList<PlayerBean> getAnyPlayer(int ID) throws Exception
	{
		Connection con = DBConnect.getConnection();
		PreparedStatement ps = con.prepareStatement("SELECT ID,FIRST_NAME,LAST_NAME,JERSEY_NUMBER FROM PLAYER WHERE ID = ?");
		ps.setInt(1, ID);
		ResultSet rs = ps.executeQuery();
		ArrayList<PlayerBean> alist = new ArrayList<PlayerBean>();
		
		while(rs.next())
		{
			PlayerBean player = new PlayerBean(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
			alist.add(player);
		}
	    return alist;
	}
	
	public void insertPlayer(PlayerBean player) throws Exception
	{
		Connection con = DBConnect.getConnection();
		PreparedStatement ps = con.prepareStatement("INSERT INTO PLAYER VALUES (?,?,?,?)");
		ps.setInt(1, player.getID());
		ps.setString(2, player.getFirst());
		ps.setString(3, player.getLast());
		ps.setInt(4, player.getJerseyNo());
		int flag = ps.executeUpdate();
		System.out.println(flag==1?"Successfully Inserted!":"Failure");

	}
	
	public void updatePlayer(PlayerBean player) throws Exception
	{
		Connection con = DBConnect.getConnection();
		String query = "UPDATE PLAYER SET FIRST_NAME = ?,LAST_NAME = ?,JERSEY_NUMBER = ? WHERE ID = ? ";
		PreparedStatement ps = con.prepareStatement(query);
		ps.setString(1, player.getFirst());
		ps.setString(2, player.getLast());
		ps.setInt(3, player.getJerseyNo());
		ps.setInt(4, player.getID());
		int flag = ps.executeUpdate();
		con.commit();
		System.out.println(flag==1?"Successfully Updated!":"Failure");
	}
	
	public void deletePlayer(PlayerBean player) throws Exception
	{
		Connection con = DBConnect.getConnection();
		String query = "DELETE PLAYER WHERE ID = ? ";
		PreparedStatement ps = con.prepareStatement(query);
		ps.setInt(1, player.getID());
		int flag = ps.executeUpdate();
		con.commit();
		System.out.println(flag==1?"Successfully Deleted!":"Failure");
	}
	
	
}
