package com.sapient.week1.question18;

import java.util.ArrayList;
import java.util.List;

public class Demo {

	public static void main(String[] args) throws Exception
	{
		System.out.println("Players");
		CricketDAO ckr = new CricketDAO();
		PlayerBean player = new PlayerBean(6,"Tejas","Parmar",25);
		
		//ckr.deletePlayer(player);
		//ckr.insertPlayer(player);
		player.setJerseyNo(5);
		ckr.updatePlayer(player);
		
		try {
			List<PlayerBean> alist = ckr.getPlayers();
			for(PlayerBean player1:alist)
			{
				System.out.println(player1);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

}
