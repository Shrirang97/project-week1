package com.sapient.week1.question18;

public class PlayerBean
{
	private int ID;
	private String First;
	private String Last;
	private int JerseyNo;
	
	public PlayerBean(int iD, String first, String last, int jerseyNo) {
		super();
		ID = iD;
		First = first;
		Last = last;
		JerseyNo = jerseyNo;
	}
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getFirst() {
		return First;
	}
	public void setFirst(String first) {
		First = first;
	}
	public String getLast() {
		return Last;
	}
	public void setLast(String last) {
		Last = last;
	}
	public int getJerseyNo() {
		return JerseyNo;
	}
	public void setJerseyNo(int jerseyNo) {
		JerseyNo = jerseyNo;
	}
	
	@Override
	public String toString() 
	{
		return ID+" "+First+" "+Last+" "+JerseyNo;
	}
	
}
